import { ResolvedFn, RejectedFn } from '../types'

// 数组每个元素都是这样
interface Interceptor<T> {
  resolved: ResolvedFn<T>
  rejected?: RejectedFn
}

// 整个都是泛型类。=》
// request【AxiosRequestConfig】和
// response【AxiosResponse】
// 的拦截器里面resolve用的类型不一样
export default class InterceptorManager<T> {
  /*interceptors存储拦截器，每个数组元素都是Interceptor泛型的接口【包括resolve和reject两个属性】
  * 即[[resolved,rejected],[resolved,rejected],[resolved,rejected]]*/
  private interceptorsList: Array<Interceptor<T> | null>

  constructor() {
    this.interceptorsList = []
  }

  use(resolved: ResolvedFn<T>, rejected?: RejectedFn): number {
    this.interceptorsList.push({
      resolved,
      rejected
    })
    return this.interceptorsList.length - 1 // use一下返回的就是id
  }

  // 传入函数进来
  forEach(fn: (interceptor: Interceptor<T>) => void): void {
    this.interceptorsList.forEach(interceptor => {
      if (interceptor !== null) {
        // 遍历拦截器，传入的参数可能要处理，于是每个拦截器作为参数传进去都处理一遍
        fn(interceptor)
      }
    })
  }

  eject(id: number): void {
    if (this.interceptorsList[id]) {
      this.interceptorsList[id] = null // 要删除拦截器，对应位置null，不能删除，因为id对应use了几次，删了的话id会乱。上面定义的时候设置联合类型null
    }
  }
}
