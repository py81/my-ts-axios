import { AxiosPromise, AxiosRequestConfig, AxiosResponse } from '../types/index'
import { parseHeaders } from '../helpers/headers'
import { AxiosError, createError } from '../helpers/error'
import { isURLSameOrigin } from '../helpers/url'
import cookie from '../helpers/cookie'
import { isFormData } from '../helpers/util'

export default function xhr(config: AxiosRequestConfig): AxiosPromise {
  // 这边相当于用promise困住，readyState没到4的时候，一直在房间里呆着。如果失败，报error，继续困住。如果成功，resolve释放出去=>外面可以用.then调用了
  return new Promise((resolve, reject) => {
    const {
      data = null,
      url,
      method,
      headers = {},
      responseType,
      timeout,
      cancelToken,
      withCredentials,
      xsrfCookieName,
      xsrfHeaderName,
      onDownloadProgress,
      onUploadProgress,
      auth,
      validateStatus
    } = config

    const request = new XMLHttpRequest()

    request.open(method!.toUpperCase(), url!, true)

    configureRequest()

    addEvents()

    processHeaders()

    processCancel()

    request.send(data)

    function configureRequest(): void {
      // 假如我设置了要求返回的是blob，那么请求的时候，requst对象就设置blob属性
      if (responseType) {
        request.responseType = responseType
      }

      if (timeout) {
        request.timeout = timeout
      }

      if (withCredentials) {
        request.withCredentials = withCredentials
      }
    }

    function addEvents(): void {
      request.onreadystatechange = function handleLoad() {
        /*0 代理被创建，但尚未调用 open() 方法。
          1 open 被调用
          2 send 被调用
          3 下载中 responseText 属性已经包含部分数据。
          4 下载完成
        */
        if (request.readyState !== 4) {
          return
        }
        if (request.status === 0) {
          return
        }

        const response: AxiosResponse = {
          data: responseType && responseType !== 'text' ? request.response : request.responseText,
          status: request.status,
          statusText: request.statusText,
          headers: parseHeaders(request.getAllResponseHeaders()),
          config,
          request
        }
        // http返回码异常处理
        handleResponse(response)
        // resolve(response)
      }

      // 网络错误的时候
      request.onerror = function handleError() {
        reject(createError('Network Error', config, null, request))
      }

      // 网络请求超时的时候
      // request.ontimeout = function handleTimeout(){
      //   reject(createError(`Timeout of ${timeout} ms exceeded`,config,'ECONNABORTED',request))
      // }
      request.ontimeout = function handleTimeout() {
        reject(
          // !!!!使用的时候以下两种方式相同!!!!
          // new AxiosError(`Timeout of ${config.timeout} ms exceeded`, config, 'ECONNABORTED', request)
          new AxiosError('timeout', config, 'ECONNABORTED', request)
        )
      }

      if (onDownloadProgress) {
        request.onprogress = onDownloadProgress
      }

      if (onUploadProgress) {
        request.upload.onprogress = onUploadProgress
      }
    }

    function processHeaders(): void {
      if (isFormData(data)) {
        delete headers['Content-Type']
      }

      if ((withCredentials || isURLSameOrigin(url!)) && xsrfCookieName) {
        const xsrfValue = cookie.read(xsrfCookieName)
        if (xsrfValue) {
          headers[xsrfHeaderName!] = xsrfValue
        }
      }

      if (auth) {
        headers['Authorization'] = 'Basic ' + btoa(auth.username + ':' + auth.password)
      }

      // 发送请求之前设置headers
      Object.keys(headers).forEach(name => {
        if (data === null && name.toLowerCase() === 'content-type') {
          delete headers[name]
        } else {
          request.setRequestHeader(name, headers[name])
        }
      })
    }

    function processCancel(): void {
      /*axios.get('/cancel/get', {
   cancelToken: new CancelToken(c => {
     cancel = c
   })
  })*/
      /*为什么cancelToken有promise属性可以点出来?
       * 因为上述注释的代码中,在实际使用过程中,配置了cancelToken这个属性的时候,实际上已经new了这个class了,所以实例化成功了.所以会有CancelTokenCls的属性可以点出来*/
      /*执行了cancel方法,状态变成resolved了,所以可以then*/
      if (cancelToken) {
        // resolve(this.reason)
        cancelToken.promise
          .then(reason => {
            request.abort()
            reject(reason)
          })
          .catch(
            /* istanbul ignore next */
            () => {
              // do nothing
            }
          )
      }
    }

    function handleResponse(response: AxiosResponse) {
      // console.log(response)
      if (!validateStatus || validateStatus(response.status)) {
        resolve(response)
      } else {
        reject(
          new AxiosError(
            `Request failed with status code ${response.status}`,
            config,
            null,
            request,
            response
          )
        )
      }
    }
  })
}
