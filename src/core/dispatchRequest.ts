import { AxiosPromise, AxiosRequestConfig, AxiosResponse } from '../types/index'
import xhr from './xhr'
import { buildURL, isAbsoluteURL, combineURL } from '../helpers/url'
import { flattenHeaders } from '../helpers/headers'
import transform from './transform'

function axios(config: AxiosRequestConfig): AxiosPromise {
  throwIfCancellationRequested(config)
  processConfig(config)
  // 为什么这里不用return new promise？  => 因为xhr函数里面已经做了，不需要做两次
  return xhr(config).then(
    res => {
      // 为什么把json字符串形式的data在这里处理，而不是xhr里面。
      // 因为xhr专注于请求的事情，headers是属于xhr请求里面的一部分
      // 数据已经返回，后面怎么处理是个性化定制，不属于xhr范围了
      return transformResponseData(res)
    },
    e => {
      if (e && e.response) {
        e.response = transformResponseData(e.response)
      }
      return Promise.reject(e)
    }
  )
}

function processConfig(config: AxiosRequestConfig): void {
  config.url = transformURL(config)
  config.data = transform(config.data, config.headers, config.transformRequest)
  config.headers = flattenHeaders(config.headers, config.method!)
}

export function transformURL(config: AxiosRequestConfig): string {
  let { url, params, paramsSerializer, baseURL } = config
  if (baseURL && !isAbsoluteURL(url!)) {
    url = combineURL(baseURL, url)
  }
  return buildURL(url!, params, paramsSerializer)
}

function transformResponseData(res: AxiosResponse): AxiosResponse {
  res.data = transform(res.data, res.headers, res.config.transformResponse)
  return res
}

/* 如果这个token已经取消过一次了=>用过一次了=>直接取消,
避免出现一种情况=>我慢慢输入搜索框,200ms输入一次,接口10s才返回,中间无数次取消的情况*/
function throwIfCancellationRequested(config: AxiosRequestConfig): void {
  if (config.cancelToken) {
    config.cancelToken.throwIfRequested()
  }
}

export default axios

export * from '../types/index'
