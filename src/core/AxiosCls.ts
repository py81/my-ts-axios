import {
  AxiosPromise,
  AxiosRequestConfig,
  AxiosResponse,
  Method,
  RejectedFn,
  ResolvedFn
} from '../types'
import dispatchRequest, { transformURL } from './dispatchRequest'
import InterceptorManager from './InterceptorManager'
import mergeConfig from './mergeConfig'

// 下方interceptors的类型
interface Interceptors {
  /*为什么是这两个?
   * 因为.interceptors.request/.interceptors.response*/
  /*为什么request类型是InterceptorManager<AxiosRequestConfig>?
   * 因为.interceptors.request.use=>要用到use方法,use方法在InterceptorManager这个class类里面写的*/
  /*为什么request的时候是<AxiosRequestConfig>?
   * 因为request的时候,resolvedFn的参数是config*/
  request: InterceptorManager<AxiosRequestConfig>
  response: InterceptorManager<AxiosResponse>
}

interface PromiseChain<T> {
  resolved: ResolvedFn<T> | ((config: AxiosRequestConfig) => AxiosPromise)
  rejected?: RejectedFn
}

export default class AxiosCls {
  /*为什么这需要添加?
   * 因为在class里面,后面用的时候需要.interceptors.request.use
   * 这样最终导出的混合axios对象,才有这么个属性可以用*/
  interceptors: Interceptors

  defaults: AxiosRequestConfig
  constructor(initConfig: AxiosRequestConfig) {
    this.defaults = initConfig
    this.interceptors = {
      request: new InterceptorManager<AxiosRequestConfig>(),
      response: new InterceptorManager<AxiosResponse>()
    }
  }

  // 函数重载
  request(url: any, config?: any): AxiosPromise {
    // url的值可能是一个单纯的字符串url【两个参数】，也可能是一个config配置【一个参数】
    if (typeof url === 'string') {
      // 有两个参数的情况下
      if (!config) {
        // 如果第二个参数没有，默认给个空对象{}
        config = {}
      }
      // 如果第二个参数有的话，那么缺少一个url，赋值就行。
      /*函数重载用
      axios('/extend/post', {
          method: 'post',
          data: {
            msg: 'hello'
          }
     })*/
      config.url = url
    } else {
      // 只有一个参数。即没有url.那么第一个参数表面上名字是url，实际上是个config对象的属性
      // 1 个参数
      /*axios({
        url: '/extend/post',
        method: 'post',
        data: {
          msg: 'hi'
        }
      })*/
      config = url
    }

    config = mergeConfig(this.defaults, config)
    config.method = config.method.toLowerCase()

    /*先定义一个链的中间,发送请求的位置*/
    const chain: PromiseChain<any>[] = [
      {
        resolved: dispatchRequest,
        rejected: undefined
      }
    ]

    // 遍历用户写的请求拦截器,如123,在中间的左边unshift上去,倒着来
    this.interceptors.request.forEach(interceptor => {
      chain.unshift(interceptor)
    })

    // 同理响应拦截器放到后面
    this.interceptors.response.forEach(interceptor => {
      chain.push(interceptor)
    })

    // 先处理第一个,直接开始链接的开始
    let promise = Promise.resolve(config)

    // [[resolved,rejected],[resolved,rejected],[resolved,rejected]]
    // 拿到第一个的resolved后,用then(resolved, rejected)继续链接下去
    while (chain.length) {
      const { resolved, rejected } = chain.shift()!
      promise = promise.then(resolved, rejected)
    }

    return promise
  }

  get(url: string, config?: AxiosRequestConfig): AxiosPromise {
    return this._requestNoData('get', url, config)
  }

  delete(url: string, config?: AxiosRequestConfig): AxiosPromise {
    return this._requestNoData('delete', url, config)
  }

  head(url: string, config?: AxiosRequestConfig): AxiosPromise {
    return this._requestNoData('head', url, config)
  }

  options(url: string, config?: AxiosRequestConfig): AxiosPromise {
    return this._requestNoData('options', url, config)
  }

  post(url: string, data?: any, config?: AxiosRequestConfig): AxiosPromise {
    return this._requestWithData('post', url, data, config)
  }

  put(url: string, data?: any, config?: AxiosRequestConfig): AxiosPromise {
    return this._requestWithData('put', url, data, config)
  }

  patch(url: string, data?: any, config?: AxiosRequestConfig): AxiosPromise {
    return this._requestWithData('patch', url, data, config)
  }
  getUri(config?: AxiosRequestConfig): string {
    config = mergeConfig(this.defaults, config)
    return transformURL(config)
  }
  _requestNoData(method: Method, url: string, config?: AxiosRequestConfig): AxiosPromise {
    return this.request(
      Object.assign(config || {}, {
        method,
        url
      })
    )
  }

  _requestWithData(
    method: Method,
    url: string,
    data?: any,
    config?: AxiosRequestConfig
  ): AxiosPromise {
    return this.request(
      Object.assign(config || {}, {
        method,
        data,
        url
      })
    )
  }
}
