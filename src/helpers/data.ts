import { isPlainObject } from './util'

export function transformRequest(data: any): any {
  if (isPlainObject(data)) { // 参数是对象的话，序列化后传递
    data = JSON.stringify(data)
  }
  return data
}


export function transformResponse(data: any): any {
  if (typeof data === 'string') {
    try {
      data = JSON.parse(data)
    } catch (e) {
      // do nothing
    }
  }
  return data
}
