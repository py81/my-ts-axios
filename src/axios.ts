import { AxiosInstance, AxiosRequestConfig, AxiosStatic } from './types'
import AxiosCls from './core/AxiosCls'
import { extend } from './helpers/util'
import defaults from './defaults'
import mergeConfig from './core/mergeConfig'

import CancelToken from './cancel/CancelToken'
import Cancel, { isCancel } from './cancel/Cancel'

// 为了创建混合对象
function createInstance(config: AxiosRequestConfig): AxiosStatic {
  // 实例化context对象
  // contex有原型方法和实例方法
  const context = new AxiosCls(config)

  // instance属于混合对象，可以函数方式/方法方式调用
  // axios({xxxx}) 是调用下面这个方法
  const instance = AxiosCls.prototype.request.bind(context)

  // axios类上的原型属性和实例属性拷贝到instance
  extend(instance, context)

  // 此时instance上面有axios({})方法和axios.get(url,config)方法
  return instance as AxiosStatic
}

const axios = createInstance(defaults)

axios.create = function create(config) {
  return createInstance(mergeConfig(defaults, config))
}

axios.CancelToken = CancelToken
axios.Cancel = Cancel
axios.isCancel = isCancel

axios.all = function all(promises) {
  return Promise.all(promises)
}

axios.spread = function spread(callback) {
  return function wrap(arr) {
    return callback.apply(null, arr)
  }
}

axios.Axios = AxiosCls

export default axios
