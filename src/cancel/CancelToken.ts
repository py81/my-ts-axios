import { CancelExecutor, Canceler, CancelTokenSource } from '../types'

import Cancel from './Cancel'

export default class CancelToken {
  promise: Promise<Cancel>
  reason?: Cancel

  /*当axios请求的时候加了canceltoken的配置,实际上已经new了一个class了,所以就会走constructor的逻辑
   * 里面构造了一个pending状态的promise,然后立即执行executor函数逻辑,
   * 在没有用cancel()之前就会卡住
   * 什么时候释放?
   * 里面有一个cancel方法,也就是调用的时候,如下代码,
   */
  //    let cancel: Canceler
  //
  // axios.get('/cancel/get', {
  //   cancelToken: new CancelToken(c => {
  //     cancel = c
  //   })
  // }).catch(function(e) {
  //   if (axios.isCancel(e)) {
  //     console.log('Request canceled')
  //   }
  // })
  //
  // setTimeout(() => {
  //   cancel()
  // }, 200)

  /* 一旦用了cancel()方法,然后promise的状态变成resolved,然后xhr.ts里面的.then才能成功.因为resolved了.
   * 所以,一旦外面构造的东西 调用了cancel方法后,就会走abort方法,然后reject[catch可以捕获到]*/

  /* if (cancelToken) {
      cancelToken.promise.then(reason => {
        request.abort()
        reject(reason)
      })
    }*/

  constructor(executor: CancelExecutor) {
    this.promise = new Promise<Cancel>(resolve => {
      // 外部调用cancel('dd')方法的时候其实就是调用下面这段逻辑,状态变成resolved了=>才能request.abort()停止
      executor(message => {
        if (this.reason) {
          return
        }
        this.reason = new Cancel(message)
        // cancelToken.promise.then(reason => {
        resolve(this.reason)
      })
    })
  }

  throwIfRequested(): void {
    if (this.reason) {
      throw this.reason
    }
  }

  static source(): CancelTokenSource {
    let cancel!: Canceler
    const token = new CancelToken(c => {
      cancel = c
    })
    /*source.cancel('Operation canceled by the user.')
     * 这个方法其实就是*/

    return {
      cancel,
      token
    }
  }
}
