import axios from '../../src/index'

// 拓展接口
// axios({
//   url: '/extend/post',
//   method: 'post',
//   data: {
//     msg: 'axois，没有.get/.request'
//   }
// })
//
// axios.request({
//   url: '/extend/post',
//   method: 'post',
//   data: {
//     msg: 'axios.request'
//   }
// })
//
// axios.get('/extend/get')
//
// axios.options('/extend/options')
//
// axios.delete('/extend/delete')
//
// axios.head('/extend/head')
//
// axios.post('/extend/post', { msg: 'post' })
//
// axios.put('/extend/put', { msg: 'put' })
//
// axios.patch('/extend/patch', { msg: 'patch' })

// 函数重载,两种参数情况
// axios({
//   url: '/extend/post',
//   method: 'post',
//   data: {
//     msg: '一个config参数'
//   }
// })
//
// axios('/extend/post', {
//   method: 'post',
//   data: {
//     msg: '两个参数，url拆分'
//   }
// })
//

// 响应数据支持泛型
interface User {
  name: string,
  age: number
}
interface ResponseData<T = any> {
  code:number,
  result:T,
  message:string,
}

function getUserAPI<T>() {
  // axios.getxxxxxx
  return axios<ResponseData<T>>('/extend/user').then(res => res.data).catch(err => console.error(err))
  // return axios('/extend/user').then(res => res.data).catch(err => console.error(err))
}

async function _search() {
  const res = await getUserAPI<User>()
  if(res){
    console.log(res.result.name)
    // console.log(res.status)
    /*正因为上面getUserAPI的return里面表达了返回是自定义接口类型ResponseData【code\result\message这样的类型】，res.后面才能跟出result方法。
    * 假如上面只是return axios('/extend/user)。那么res只能.出AxiosResponse的类型*/
    /*我的理解
调用 `getUser<User>` 的时候，上面定义了`getUser<T>` 此时T为User
当`return axios<ResponseData<T>>`的时候，返回的是`AxiosPromise<T>`，此时promise这里面的T是`ResponseData<T>`，,所以await之后的res返回，能够推断出是个`ResponseData<T>`类型，因此res能提示出.result/.code*/
  }
  // user 被推断出为
  // {
  //  code: number,
  //  result: { name: string, age: number },
  //  message: string
  // }
}
_search()


